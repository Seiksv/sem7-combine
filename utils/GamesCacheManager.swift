    //
    //  SeriesCacheManager.swift
    //  sem6 maoo
    //
    //  Created by Seiks on 13/3/22.
    //

import Foundation
import Reachability
import CodableCache

final class GamesCacheManager {
    
    let cache: CodableCache<Games>
    
    init(cacheKey: AnyHashable) {
        cache = CodableCache<Games>(key: cacheKey)
    }
    
    func getEpisodes() -> Games? {
        return cache.get()
    }
    
    func set(series: Games) throws {
        try cache.set(value: series)
    }
}
