    //
    //  SeriesCacheManager.swift
    //  sem6 maoo
    //
    //  Created by Seiks on 13/3/22.
    //

import Foundation
import Reachability
import CodableCache

final class TeamsCacheManager {
    
    let cache: CodableCache<Teams>
    
    init(cacheKey: AnyHashable) {
        cache = CodableCache<Teams>(key: cacheKey)
    }
    
    func getEpisodes() -> Teams? {
        return cache.get()
    }
    
    func set(series: Teams) throws {
        try cache.set(value: series)
    }
}
