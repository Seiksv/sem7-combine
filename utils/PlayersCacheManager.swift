    //
    //  SeriesCacheManager.swift
    //  sem6 maoo
    //
    //  Created by Seiks on 13/3/22.
    //

import Foundation
import Reachability
import CodableCache

final class PlayersCacheManager {
    
    let cache: CodableCache<Players>
    
    init(cacheKey: AnyHashable) {
        cache = CodableCache<Players>(key: cacheKey)
    }
    
    func getEpisodes() -> Players? {
        return cache.get()
    }
    
    func set(series: Players) throws {
        try cache.set(value: series)
    }
}
