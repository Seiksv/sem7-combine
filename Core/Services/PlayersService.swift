//
//  PlayersController.swift
//  sem7 combine
//
//  Created by Seiks on 17/3/22.
//

import Foundation
import Combine
protocol PlayersControllerProtocol: AnyObject {
    var networkController: NetworkControllerProtocol { get }
    func getPlayers(page: Int) -> AnyPublisher<Players, Error>
    func getTeams(page: Int) -> AnyPublisher<Teams, Error>
    func getGames(page: Int) -> AnyPublisher<Games, Error>
}

final class PlayersService: PlayersControllerProtocol {
    
    let networkController: NetworkControllerProtocol
    
    init(networkController: NetworkControllerProtocol) {
        self.networkController = networkController
    }
    
    func getPlayers(page: Int) -> AnyPublisher<Players, Error> {
        let endpoint = Endpoint.init(path: NetworkConstants.players,
                                     queryItems: [URLQueryItem(name: NetworkConstants.pageParam, value: "\(page)")])
        
        return networkController.get(type: Players.self, url: endpoint.url)
    }
    
    func getTeams(page: Int) -> AnyPublisher<Teams, Error> {
        
        let endpoint = Endpoint.init(path: NetworkConstants.teams,
                                     queryItems: [URLQueryItem(name: NetworkConstants.pageParam, value: "\(page)")])
        return networkController.get(type: Teams.self, url: endpoint.url)
    }
    
    func getGames(page: Int) -> AnyPublisher<Games, Error> {
        let endpoint = Endpoint.init(path: NetworkConstants.games,
                                     queryItems: [URLQueryItem(name: NetworkConstants.pageParam, value: "\(page)")])
        return networkController.get(type: Games.self, url: endpoint.url)
    }
}
