//
//  Meta.swift
//  sem7 combine
//
//  Created by Seiks on 18/3/22.
//

import Foundation
struct Meta: Codable {
    let totalPages: Int?
    let currentPage: Int?
    let nextPage: Int?
    let perPage: Int?
    let totalCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case totalPages = "total_pages"
        case currentPage = "current_page"
        case nextPage = "next_page"
        case perPage = "per_page"
        case totalCount = "total_count"
    }
}
