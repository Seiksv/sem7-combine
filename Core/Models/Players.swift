//
//  Players.swift
//  sem7 combine
//
//  Created by Seiks on 17/3/22.
//

import Foundation
struct Players: Codable {
    var data: [Player]?
    let meta: Meta?
}

struct Player: Codable {
    let id: Int?
    let firstName: String?
    let lastName: String?
    let position: String?
    let heightFeet: Int?
    let heightInches: Int?
    let weightPounds: Int?
    let team: Team?
    
    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case position
        case heightFeet = "height_feet"
        case heightInches = "height_inches"
        case weightPounds = "weight_pounds"
        case team
    }
}
