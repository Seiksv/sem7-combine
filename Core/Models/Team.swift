//
//  Team.swift
//  sem7 combine
//
//  Created by Seiks on 17/3/22.
//

import Foundation
struct Team: Codable {
    
    let id: Int?
    let abbreviation: String?
    let city: String?
    let conference: String?
    let division: String?
    let full_name: String?
    let name: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case abbreviation = "abbreviation"
        case city = "city"
        case conference = "conference"
        case division = "division"
        case full_name = "full_name"
        case name = "name"
    }
    
    enum Conference: Decodable {
        case all
        case chocolate
        case hard
    }
}

struct Teams: Codable {
    var data: [Team]?
}


extension Team.Conference: CaseIterable { }

extension Team.Conference: RawRepresentable {
    typealias RawValue = String
    
    init?(rawValue: RawValue) {
        switch rawValue {
        case "All": self = .all
        case "East": self = .chocolate
        case "West": self = .hard
        default: return nil
        }
    }
    
    var rawValue: RawValue {
        switch self {
        case .all: return "All"
        case .chocolate: return "East"
        case .hard: return "West"
        }
    }
}
