
    //
    //  Players.swift
    //  Games
    //
    //  Created by Seiks on 17/3/22.
    //

import Foundation
struct Games: Codable {
    var data: [Game]?
    let meta: Meta?
}

struct Game: Codable {
    let id: Int?
    let homeTeamScore: Int?
    let period: Int?
    let postseason: Bool?
    let season: Int?
    let status: String?
    let time: String?
    let visitorTeamScore: Int?
    let date: String?
    let homeTeam: Team?
    let visitorTeam: Team?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case homeTeamScore = "home_team_score"
        case period = "period"
        case postseason = "postseason"
        case season = "season"
        case status = "status"
        case time = "time"
        case visitorTeamScore = "visitor_team_score"
        case date = "date"
        case homeTeam = "home_team"
        case visitorTeam = "visitor_team"
    }
}
