//
//  NetworkConstants.swift
//  sem7 combine
//
//  Created by Seiks on 20/3/22.
//

import Foundation
struct NetworkConstants {
    
        // Base URL
    static let baseURL = "www.balldontlie.io"
    static let players = "/players"
    static let teams = "/teams"
    static let games = "/games"
    static let api = "/api/v1" 
    
    static let pageParam = "page"
    
        //Headers del request
    enum HTTPHeaderFieldKey: String {
        case authorization = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
        //Tipo de formato
    enum HTTPHeaderFieldValue: String {
        case json = "application/json"
    }
}
