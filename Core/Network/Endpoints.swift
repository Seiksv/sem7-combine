//
//  Endpoints.swift
//  sem7 combine
//
//  Created by Seiks on 17/3/22.
//

import Foundation
struct Endpoint {
    var path: String
    var queryItems: [URLQueryItem] = []
}

extension Endpoint {
    static var players: Self {
        return Endpoint(path: NetworkConstants.players)
    }
    static var teams: Self {
        return Endpoint(path: NetworkConstants.teams)
    }
    static var games: Self {
        return Endpoint(path: NetworkConstants.games)
    }
}

extension Endpoint {
    var url: URL {
        var components = URLComponents()
        components.scheme = "https"
        components.host = NetworkConstants.baseURL
        components.path = NetworkConstants.api + path
        components.queryItems = queryItems
        
        guard let url = components.url else {
            preconditionFailure("Invalid URL components: \(components)")
        }
        
        return url
    }
}
