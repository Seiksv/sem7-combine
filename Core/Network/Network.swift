//
//  Network.swift
//  sem7 combine
//
//  Created by Seiks on 17/3/22.
//

import Foundation
import Combine

protocol NetworkControllerProtocol: AnyObject {
    typealias Headers = [String: Any]
    
    func get<T>(type: T.Type,
                url: URL
    ) -> AnyPublisher<T, Error> where T: Decodable
}


class NetworkController: NetworkControllerProtocol {
    func get<T: Decodable>(type: T.Type, url: URL) -> AnyPublisher<T, Error> {
        return URLSession.shared.dataTaskPublisher(for: URLRequest(url: url))
            .map(\.data)
            .decode(type: T.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}

