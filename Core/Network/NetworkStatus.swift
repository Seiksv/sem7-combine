    //
    //  NetworkStatus.swift
    //  sem6 maoo
    //
    //  Created by Seiks on 13/3/22.
    //

import Foundation
import Reachability

class NetworkStatus {
    
    let reachability = try! Reachability()
    var isConnected = true
    
    
    func buildNotifier(){
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection == .wifi || reachability.connection == .cellular {
            isConnected = true
            
        } else {
            isConnected = false
        }
    }
    
    func getConectionStatus() -> Bool{
        if reachability.connection != .unavailable {
            isConnected = true
            return true
        } else {
            isConnected = false
            return false
        }
    }
}
