//
//  ViewController.swift
//  sem7 combine
//
//  Created by Seiks on 17/3/22.
//

import UIKit
import Combine

class PlayersViewController: UIViewController, UITableViewDelegate, UIScrollViewDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchFooter: SearchFooter!
    @IBOutlet weak var searchFooterBottomConstraint: NSLayoutConstraint!
    
    let playerViewModel = PlayersViewModel()
    let searchController = UISearchController(searchResultsController: nil)
    let playersManager = PlayersCacheManager(cacheKey: "playersManager")
    var subscriptions = Set<AnyCancellable>()
    var paginating = false
    var playersArrayData: [Player]? = []
    var playersArrayDataFiltered: [Player]? = []
    var scrollView = UIScrollView()
    var stackView = UIStackView()
    var isSearchBarEmpty: Bool { return searchController.searchBar.text?.isEmpty ?? true }
    var isFiltering: Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Players"
        configSearch()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func getData() {
        if !paginating {
            Toast.show(message: "Fetchign data", controller: self)
            paginating = true
            let data = playerViewModel.getAllPlayers()
            if let network = playerViewModel.networkStatus, network.getConectionStatus() {
                data.receive(on: DispatchQueue.main)
                    .sink(receiveCompletion: { (completion) in
                        switch completion {
                        case .failure(let error):
                            print("Error al obtener data: \(error)")
                        case .finished:
                            print("Finalizado ")
                        }
                    }, receiveValue: { [weak self] data in
                        self?.playersArrayData?.append(contentsOf: data.data!)
                        self?.playersArrayDataFiltered?.append(contentsOf: data.data!)
                        self?.reloadTable()
                        self?.paginating = false
                        
                    })
                    .store(in: &subscriptions)
            } else {
                if let serieCache = playersManager.getEpisodes()  {
                    self.playersArrayData? = serieCache.data!
                    self.playersArrayData? = serieCache.data!
                    self.reloadTable()
                    self.paginating = false
                    Toast.show(message: "Contenido cargado desde cache", controller: self)
                } else {
                    Toast.show(message: "No hay contenido guardado desde cache", controller: self)
                }
            }
        }
    }
    
    func reloadTable(){
        self.tableView.dataSource = self
        self.tableView.reloadData()
        self.tableView.delegate = self
    }
    
    func configSearch() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Players"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.searchBar.delegate = self
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
        if let data = playersArrayData {
            playersArrayDataFiltered? = data.filter { (seriesDetail: Player) -> Bool in
                if isSearchBarEmpty {
                    return true
                } else {
                    return  seriesDetail.firstName!.lowercased().contains(searchText.lowercased())
                }
            }
        }
      
        tableView.reloadData()
    }
    
    func handleKeyboard(notification: Notification) {
        guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
            searchFooterBottomConstraint.constant = 0
            view.layoutIfNeeded()
            return
        }
        
        guard
            let info = notification.userInfo,
            let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else {
            return
        }
        
        let keyboardHeight = keyboardFrame.cgRectValue.size.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.searchFooterBottomConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
        })
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset <= 10.0 {
            
            if !paginating {
                print("Paginando")
                getData()
            }
        }
    }
}

extension PlayersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        guard let countFiltered = playersArrayDataFiltered?.count else { return 0 }
        guard let count = playersArrayData?.count else { return 0 }
       
        if isFiltering {
            self.searchFooter.setIsFilteringToShow(filteredItemCount: countFiltered,
                                                       of: count)
            return countFiltered
        } else {
            searchFooter.setNotFiltering()
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! seriesTableViewCell
        let teamp: Player?
        
        if isFiltering {
            teamp = playersArrayDataFiltered?[indexPath.row]
        } else {
            teamp = playersArrayData?[indexPath.row]
        }
        
        cell.playerName.text = teamp?.firstName
        cell.playerPosition.text = teamp?.position ?? "N/D"
        cell.playerTeam.text = teamp?.team?.abbreviation ?? "N/D"
        cell.playerHeightFeet.text! = "Height: \(teamp?.heightFeet ?? 0)"
        cell.playerHeightInches.text! = "Height: \(teamp?.heightInches ?? 0)"
        cell.playerWeight.text! = "Weight: \(teamp?.weightPounds ?? 0)"
        
        return cell
    }
}

extension PlayersViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}

extension PlayersViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!)
    }
}
