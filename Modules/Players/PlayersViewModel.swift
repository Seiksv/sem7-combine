//
//  PlayersViewModel.swift
//  sem7 combine
//
//  Created by Seiks on 17/3/22.
//

import Foundation
import Combine
class PlayersViewModel {
    let networkController = NetworkController()
    var playersService: PlayersService?
    var page: Int = 0
    var networkStatus: NetworkStatus? = nil
    
    init(playersController: PlayersService = PlayersService(networkController: NetworkController()), networkController: NetworkStatus = NetworkStatus()){
        self.playersService = playersController
        self.networkStatus = networkController
        networkStatus?.buildNotifier()
    }
    
    func getAllPlayers() -> AnyPublisher<Players, Error>  {
        page += 1
        return playersService!.getPlayers(page: page)
    }
}
