    //
    //  Cell.swift
    //  sem6 maoo
    //
    //  Created by Seiks on 12/3/22.
    //

import Foundation
import UIKit

class seriesTableViewCell: UITableViewCell{
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerPosition: UILabel!
    @IBOutlet weak var playerTeam: UILabel!
    @IBOutlet weak var playerHeightFeet: UILabel!
    @IBOutlet weak var playerHeightInches: UILabel!
    @IBOutlet weak var playerWeight: UILabel!
}

class gamesTableViewCell: UITableViewCell{
    @IBOutlet weak var playerDate: UILabel!
    @IBOutlet weak var playerPosition: UILabel!
    @IBOutlet weak var playerTeam: UILabel!
    @IBOutlet weak var playerHeightFeet: UILabel!
    @IBOutlet weak var playerHeightInches: UILabel!
    @IBOutlet weak var playerWeight: UILabel!
}
