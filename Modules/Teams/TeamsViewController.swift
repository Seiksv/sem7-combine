    //
    //  ViewController.swift
    //  sem7 combine
    //
    //  Created by Seiks on 17/3/22.
    //

import UIKit
import Combine

class TeamsViewController: UIViewController, UITableViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchFooter: SearchFooter!
    @IBOutlet weak var searchFooterBottomConstraint: NSLayoutConstraint!
    let teamsManager = TeamsCacheManager(cacheKey: "teamManager")
    let searchController = UISearchController()
    let teamsViewModel = TeamsViewModel()
    let sortByConference = {(teamA: Team, teamB: Team) in
        teamA.name! < teamB.name!
    }
    var subscriptions = Set<AnyCancellable>()
    var paginating = false
    var scrollView = UIScrollView()
    var stackView = UIStackView()
    var page = 1
    var isSearchBarEmpty: Bool { return searchController.searchBar.text?.isEmpty ?? true }
    var teamsArrayData: [Team]? = []
    var teamsArrayDataFiltered: [Team]? = []
    var isFiltering: Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Teams"
        configSearch()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func getData() {
        if !paginating {
            Toast.show(message: "Fetchign data", controller: self)
            paginating = true
            let data = teamsViewModel.getAllteams()
            if let network = teamsViewModel.networkStatus, network.getConectionStatus() {
                data.receive(on: DispatchQueue.main)
                    .sink(receiveCompletion: { (completion) in
                        switch completion {
                        case .failure(let error):
                            print("Error al obtener data: \(error)")
                        case .finished:
                            print("Finalizado ")
                        }
                    }, receiveValue: { [weak self] data in
                        self?.teamsArrayData?.append(contentsOf: data.data!)
                        self?.teamsArrayDataFiltered?.append(contentsOf: data.data!)
                        self?.reloadTable()
                        self?.paginating = false
                        
                    })
                    .store(in: &subscriptions)
            } else {
                if let serieCache = teamsManager.getEpisodes()  {
                    self.teamsArrayData? = serieCache.data!
                    self.teamsArrayDataFiltered? = serieCache.data!
                    self.reloadTable()
                    self.paginating = false
                    Toast.show(message: "Contenido cargado desde cache", controller: self)
                } else {
                    Toast.show(message: "No hay contenido guardado desde cache", controller: self)
                }
            }
        }
    }
    
    func reloadTable(){
        self.tableView.dataSource = self
        self.tableView.reloadData()
        self.tableView.delegate = self
    }
    
    func configSearch() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Series"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.searchBar.delegate = self
        searchController.searchBar.scopeButtonTitles = Team.Conference.allCases.map { $0.rawValue }

    }
    
    func filterContentForSearchText(_ searchText: String,
                                    category: Team.Conference? = nil) {
        teamsArrayData = teamsArrayData!.sorted(by: sortByConference)
        if let data = teamsArrayData {
            teamsArrayDataFiltered? = data.filter { (seriesDetail: Team) -> Bool in
                let doesCategoryMatch = category == .all || seriesDetail.conference! == category?.rawValue
                
                if isSearchBarEmpty {
                    return doesCategoryMatch
                } else {
                    return doesCategoryMatch && seriesDetail.name!.lowercased().contains(searchText.lowercased())
                }
            }
        }
        tableView.reloadData()
    }
    
    func handleKeyboard(notification: Notification) {
        guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
            searchFooterBottomConstraint.constant = 0
            view.layoutIfNeeded()
            return
        }
        
        guard
            let info = notification.userInfo,
            let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else {
            return
        }
        
        let keyboardHeight = keyboardFrame.cgRectValue.size.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.searchFooterBottomConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
        })
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 10.0 {
            print("Llego al final")
            if !paginating {
                print("Paginando")
                getData()
            }
        }
    }
}

extension TeamsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        guard let countFiltered = teamsArrayDataFiltered?.count else { return 0 }
        guard let count = teamsArrayData?.count else { return 0 }
        
        if isFiltering {
            self.searchFooter.setIsFilteringToShow(filteredItemCount: countFiltered,
                                                   of: count)
            return countFiltered
        } else {
            searchFooter.setNotFiltering()
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! seriesTableViewCell
        let teamp: Team?
        
        if isFiltering {
            teamp = teamsArrayDataFiltered?[indexPath.row]
        } else {
            teamp = teamsArrayData?[indexPath.row]
        }
        
        //seal.fulfill(UIImage(named: "noImage")!)
        cell.playerName.text = teamp?.full_name
        cell.playerPosition.text = teamp?.abbreviation
        cell.playerTeam.text = teamp?.city
        cell.playerHeightFeet.text = teamp?.conference
        cell.playerHeightInches.text = teamp?.division
        cell.playerWeight.text = teamp?.name
        
        return cell
    }
}


extension TeamsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        
        let category = Team.Conference(rawValue:
                                        searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex])
        filterContentForSearchText(searchBar.text!, category: category)
    }
}

extension TeamsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        let category = Team.Conference(rawValue:
                                        searchBar.scopeButtonTitles![selectedScope])
        
        filterContentForSearchText(searchBar.text!, category: category)
    }
}
