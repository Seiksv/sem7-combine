    //
    //  teamsViewModel.swift
    //  sem7 combine
    //
    //  Created by Seiks on 17/3/22.
    //

import Foundation
import Combine
class TeamsViewModel {
    let networkController = NetworkController()
    var teamsService: PlayersService?
    var page: Int = 0
    var networkStatus: NetworkStatus? = nil
    
    init(teamsController: PlayersService = PlayersService(networkController: NetworkController()), networkController: NetworkStatus = NetworkStatus()){
        self.teamsService = teamsController
        self.networkStatus = networkController
        networkStatus?.buildNotifier()
    }
    
    func getAllteams() -> AnyPublisher<Teams, Error>  {
        page += 1
        return teamsService!.getTeams(page: page)
    }
}
