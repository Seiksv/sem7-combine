    //
    //  ViewController.swift
    //  sem7 combine
    //
    //  Created by Seiks on 17/3/22.
    //

import UIKit
import Combine

class GamesViewController: UIViewController, UITableViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchFooter: SearchFooter!
    @IBOutlet weak var searchFooterBottomConstraint: NSLayoutConstraint!
    let gamesViewModel = GamesViewModel()
    let gamesManager = GamesCacheManager(cacheKey: "gamesManager")
    let searchController = UISearchController(searchResultsController: nil)
    var subscriptions = Set<AnyCancellable>()
    var paginating = false
    var scrollView = UIScrollView()
    var stackView = UIStackView()
    var page = 1
    var isSearchBarEmpty: Bool { return searchController.searchBar.text?.isEmpty ?? true }
    var gamesArrayData: [Game]? = []
    var gamesArrayDataFiltered: [Game]? = []
    var isFiltering: Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Games"
        configSearch()
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func getData() {
        if !paginating {
            Toast.show(message: "Fetchign data", controller: self)
            paginating = true
            let data = gamesViewModel.getAllGames()
            if let network = gamesViewModel.networkStatus, network.getConectionStatus() {
                data.receive(on: DispatchQueue.main)
                    .sink(receiveCompletion: { (completion) in
                        switch completion {
                        case .failure(let error):
                            print("Error al obtener data: \(error)")
                        case .finished:
                            print("Finalizado ")
                        }
                    }, receiveValue: { [weak self] data in
                        
                        self?.gamesArrayData?.append(contentsOf: data.data!)
                        self?.gamesArrayData = self?.gamesArrayData!.sorted(by: self!.sortByDate)
                        self?.gamesArrayDataFiltered?.append(contentsOf: data.data!)
                        try? self?.gamesManager.set(series: data)
                        self?.reloadTable()
                        self?.paginating = false
                    })
                    .store(in: &subscriptions)
            } else {
                if let serieCache = gamesManager.getEpisodes()  {
                    self.gamesArrayData? = serieCache.data!
                    self.gamesArrayData = serieCache.data!.sorted(by: sortByDate)
                    self.gamesArrayDataFiltered? = serieCache.data!
                    self.reloadTable()
                    self.paginating = false
                    Toast.show(message: "Contenido cargado desde cache", controller: self)
                } else {
                    Toast.show(message: "No hay contenido guardado desde cache", controller: self)
                }
            }
        }
    }
    
    func reloadTable(){
        self.tableView.dataSource = self
        self.tableView.reloadData()
        self.tableView.delegate = self
    }
    
    func configSearch() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Series"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.searchBar.delegate = self
    }
    
    func filterContentForSearchText(_ searchText: String) {
       
        if let data = gamesArrayData {
            gamesArrayDataFiltered? = data.filter { (seriesDetail: Game) -> Bool in
                if isSearchBarEmpty {
                    return true
                } else {
                    return  seriesDetail.date!.lowercased().contains(searchText.lowercased())
                }
            }
        }
        
        tableView.reloadData()
    }
    
    func handleKeyboard(notification: Notification) {
        guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
            searchFooterBottomConstraint.constant = 0
            view.layoutIfNeeded()
            return
        }
        
        guard
            let info = notification.userInfo,
            let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else {
            return
        }
        
        let keyboardHeight = keyboardFrame.cgRectValue.size.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.searchFooterBottomConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
        })
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 10.0 {
            if !paginating {
                print("Paginando")
                getData()
            }
        }
    }
    
    func convertDateFormat(inputDate: String) -> String {
        let olDateFormatter = DateFormatter()
        olDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let oldDate = olDateFormatter.date(from: inputDate)
        let convertDateFormatter = DateFormatter()
        convertDateFormatter.dateFormat = "MMM dd yyyy"
        return convertDateFormatter.string(from: oldDate!)
    }

    let sortByDate = {(gameA: Game, gameB: Game) -> Bool in
        var dateA: Date?
        var dateB: Date?
        if let stringDate = gameA.date {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            let dateObj: Date? = dateFormatterGet.date(from: stringDate)
            dateA = dateObj
        }
        if let stringDate = gameB.date {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            let dateObj: Date? = dateFormatterGet.date(from: stringDate)
            dateB = dateObj
        }
        if dateA != nil && dateB != nil{
            return dateA! > dateB!
        } else {
            return false
        }
        
    }
}

extension GamesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        guard let countFiltered = gamesArrayDataFiltered?.count else { return 0 }
        guard let count = gamesArrayData?.count else { return 0 }
        
        if isFiltering {
            self.searchFooter.setIsFilteringToShow(filteredItemCount: countFiltered,
                                                   of: count)
            return countFiltered
        } else {
            searchFooter.setNotFiltering()
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! seriesTableViewCell
        let game: Game?
        
        if isFiltering {
            game = gamesArrayDataFiltered?[indexPath.row]
        } else {
            game = gamesArrayData?[indexPath.row]
        }
        
        if let date = game?.date {
            cell.playerName.text = convertDateFormat(inputDate: date)
        }
        if let visitorTeamScorep = game?.visitorTeamScore {
            cell.playerPosition.text = "\(visitorTeamScorep)"
        }
        if let visitorTeamScorep = game?.visitorTeam?.full_name {
            cell.playerTeam.text = "\(visitorTeamScorep)"
        }
        if let visitorTeamScorep = game?.homeTeam?.full_name {
            cell.playerHeightFeet.text = "\(visitorTeamScorep)"
        }
        if let visitorTeamScorep = game?.homeTeamScore {
            cell.playerHeightInches.text = "\(visitorTeamScorep)"
        }
        if let visitorTeamScorep = game?.status {
            cell.playerWeight.text = "\(visitorTeamScorep)"
        }
       
        return cell
    }
}

extension GamesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}

extension GamesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!)
    }
}
